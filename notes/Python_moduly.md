# ORM
* [SQLAlchemy](http://www.sqlalchemy.org/)
* [Pony](https://ponyorm.com/)
* [Peewee ORM](https://peewee.readthedocs.io/en/latest/)
* [SQLObject ORM](http://sqlobject.org/)

# Szyfrowanie
* paramiko - moduł pracujący jako klient SSH
* pycrypto - szyfrowanie (to jest częściej używane od pycryptodome) http://pythonhosted.org/pycrypto/
* simple-crypt - (simplecrypt) ułatione szyfrowanie z wykorzystaniem pycrypto https://pypi.python.org/pypi/simple-crypt pip install simple-crypt
* pycryptodome / pycryptodomex - szyfrowanie https://pycryptodome.readthedocs.io/en/latest/src/examples.html#encrypt-data-with-aes

# Narzędzia systemowe
* [pyinotify](https://github.com/seb-m/pyinotify) - monitorowanie zmian w systemie plików
