#!/bin/bash

for file in *.md
do
  base="${file%.*}"
  pandoc $file -t html -o $base.html
done
