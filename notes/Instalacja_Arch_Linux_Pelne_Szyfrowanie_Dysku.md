# Instalacja Arch Linux z pełnym szyfrowaniem dysku
- https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS

## Przydatne narzędzia
- `lsblk` - wyświetla wszystkie dyski
- `blkid` - wyświetla UUID dysków

## Połączenie z siecią WiFi
```
# wifi-menu
```

## Uzyskanie adresu IP za pomocą DHCP
Adres IP możemy uzyskać za pomocą polecenia `dhcpcd` w przypadku połączenia
kablowego.

Lista interfejsów sieciowych
```
# ip a
```

Pobranie adresu IP za pomocą `dhcpcd`
```
#  dhcpcd
```

lub podając nazwę interfejsu

```
# dhcpcd enp0s25
```

## Aktualizacja listy serwerów lustrzanych
```
# pacman -Sy
# pacman -S  reflector
# reflector --verbose --country 'Poland' -l 10 -p http --sort rate \
--save /etc/pacman.d/mirrorlist
```

## Lista dysków
```
# lsblk
AME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda       8:0    0 119,2G  0 disk 
├─sda1    8:1    0 110,5G  0 part 
└─sda2    8:2    0   8,8G  0 part 
sdb       8:16   0 119,2G  0 disk 
└─sdb1    8:17   0 119,2G  0 part /
mmcblk0 179:0    0  14,9G  0 disk
```

## Nadpisanie przypadkowymi znakami dysku /dev/sda
```
# shred -vfz --random-source=/dev/urandom -n 1 /dev/sda
```

## Formatowanie partycji
```
# parted -s /dev/sda mklabel msdos
# parted -s /dev/sda mkpart primary 2048s 100%
```

## Ustawienie dysku /dev/sda jako startowego
```
# parted -s /dev/sda set 1 boot on
```

## Formatujemy partycję lvm
```
# cryptsetup -c aes-xts-plain64 -y --use-random --key-size 512 luksFormat /dev/sda1
```

Żeby operacja została potwierdzona należy wpisać słowo YES oraz dwukrotnie
podać hasło.

## Otwieramy zaszyfrowany wolumin i nazywamy go lvm
```
# cryptsetup luksOpen /dev/sda1 lvm
```
Będzie on dostępny jako `/dev/mapper/lvm`

## Wskazujemy fizyczny wolumin LVM na /dev/mapper/lvm
```
# pvcreate /dev/mapper/lvm
```

## Tworzymy gruę o nazwie vg
```
# vgcreate vg /dev/mapper/lvm
```

## Tworzymy logiczne woluminy
```
# lvcreate -L 4G vg -n swap
# lvcreate -L 1G vg -n boot
# lvcreate -l +100%FREE vg -n root
```

## Tworzymy partycję wymiany
```
# mkswap -L swap /dev/mapper/vg-swap
```

## Tworzymy potrzebne systemy plików czyli /boot i /
```
# mkfs.ext4 /dev/mapper/vg-root
# mkfs.ext4 /dev/mapper/vg-boot
```

## Montowanie partycji / i /boot
```
# mount /dev/mapper/vg-root /mnt
# mkdir /mnt/boot
# mount /dev/mapper/vg-boot /mnt/boot
```

## Instalacja podstawowych pakietów
```
# pacstrap -i /mnt base base-devel dialog wpa_supplicant
```

## Generowanie pliku /etc/fstab
```
# genfstab -pU /mnt >> /mnt/etc/fstab
```

## Montowanie /run
```
# mkdir /mnt/hostrun
# mount --bind /run /mnt/hostrun
```

## Przejście do instalowanego systemu /mnt
```
# arch-chroot /mnt
```

## Montowanie /run/lvm
```
# mkdir /run/lvm
# mount --bind /hostrun/lvm /run/lvm
```

## Ustawienie lokalizacji
W pliku `/etc/locale.gen` dodajemy linię `pl_PL.UTF-8 UTF-8` a następnie
uruchamiamy polecenie `locale-gen`.
```
# vi /etc/locale.gen
# locale-gen
```

## Ustawienie strefy czasowej
```
# ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
```

```
# hwclock --systohc
```

## Lokalizacja systemu
W pliku `/etc/locale.conf` dodajemy:
```
LANG=pl_PL.UTF-8
```

## Ustawienie nazwy systemu
```
# echo 'nazwa' > /etc/hostname
```

## Instalacja dodatkowych paczek
```
# pacman -S grub-bios os-prober dialog wpa_supplicant net-tools linux-headers \
ranger tlp acpi_call openssh sudo git vim mupdf
```

## Nadanie hasła dla użytkownika root i dodanie użytkownika
```
# passwd
# useradd -m -s /bin/bash -G wheel -g users nazwa_uzytkownika
# passwd nazwa_uzytkownika
```

## Modyfikacja /etc/mkinitcpio.conf
Modyfikacja dotyczy HOOKS należy dodać `encrypt` oraz `lvm2` przed
`filesystems`
```
HOOKS=(base udev autodetect modconf block encrypt lvm2 filesystems keyboard fsck)
```

## Utworzenie obrazu init
```
# mkinitcpio -p linux
```

## Hasło do programu rozruchowego GRUB
W pliku `/etc/default/grub` należy dodać `cryptdevice` w linii
GRUB_CMDLINE_LINUX
```
GRUB_CMD_LINE_LINUX="cryptdevice=/dev/sda1:lvm"
```

Można też użyć UUID dysku, UUID można sprawdzić za pomocą polecenia `blkid /dev/sda1`
```
GRUB_CMD_LINE_LINUX="cryptdevice=UUID=2345349-2344-2222-2222-223482394:lvm"
```

A następnie włączyć CRYPTODISK
```
GRUB_ENABLE_CRYPTODISK=y
```

## Instalacja grub
```
# grub-install /dev/sda
```

## Aktualizacja konfiguracji grub
```
# grub-mkconfig -o /boot/grub/grub.cfg
```

## Odmontowanie partycji i restart systemu
```
# exit
# umount /mnt
# reboot
```

## Dodanie klucza szyfrującego
Jak można zauważyć po restarcie należy podać dwukrotnie hasło, można tego
uniknąć dodając klucz. Kolejne czynności należy wykonać po restarcie.

UWAGA! Po uruchomieniu systemu klucz będzie na odszyfrowanym nośniku.
Jeśli nie chcesz obniżać bezpieczeństwa swojego systemu nie korzystaj 
z tej opcji. 

```
# dd bs=512 count=4 if=/dev/urandom of=/crypto_keyfile.bin
```

```
# cryptsetup luksAddKey /dev/sda1 /crypto_keyfile.bin
```

W pliku `/etc/mkinitcpio.conf` dodajemy:
```
FILES=(/crypto_keyfile.bin)
```

Teraz generujemy `initrd`
```
# mkinitcpio -p linux
```

Dodatkowo możemy zabezpieczyć dostęp do klucza oraz katalogu /boot
```
# chmod 000 /crypto_keyfile.bin
# chmod -R g-rwx,o-rwx /boot
```


## Źródła informacji
* [Arch Wiki GRUB](https://wiki.archlinux.org/index.php/GRUB#Root_partition)
* [https://www.howtoforge.com/tutorial/how-to-install-arch-linux-with-full-disk-encryption/](https://www.howtoforge.com/tutorial/how-to-install-arch-linux-with-full-disk-encryption/)

