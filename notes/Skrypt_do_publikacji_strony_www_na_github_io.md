# Skrypt do publikacji strony WWW na github.io

Skrypt Automatycznie publikuje zmiany na GitHub Pages, wygenerowane przez
Pelican Static Site Generator - https://blog.getpelican.com/

```bash
#!/usr/bin/env bash

SRC=/home/user/git/github/static/site/output/
DST=/home/user/git/github/wredny.github.io/

rsync -av --delete --exclude=.git $SRC $DST

cd $DST
git add -A
git commit -m $(date +%F_%R)
git push
```