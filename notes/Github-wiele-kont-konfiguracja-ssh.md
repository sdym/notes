# Konfiguracja SSH dla kilku kont GitHub

Jako że github nie pozwala na zaimportowanie jednego klucza SSH do kilku kont, musimy wygenerować osobne klucze. Więcej na temat autoryzacji można przeczytać na stronie pomocy https://help.github.com/categories/authenticating-to-github

```
$ ssh-keygen -t rsa -b 4096 -C "your_email@example.com" -f ~/.ssh/klucze/user-1-github
$ ssh-keygen -t rsa -b 4096 -C "your_email@example.com" -f ~/.ssh/klucze/user-2-github
```

Powyższe polecenia utworza 4 pliki:
```
user-1-github
user-1-github.pub
user-2-github
user-2-github.pub
```

Dodanie kluczy do agenta SSH w przypadku kiedy używamy jakiegoś środowiska graficznego typu GNOME czy KDE posiadają one swojego agenta. Jeśli używamy konsoli systemu Linux należy najpierw takiego agenta uruchomić.

Uruchomienie agenta w konsoli
```
$ eval "$(ssh-agent -s)"
```

Dodanie kluczy
```
$ ssh-add ~/.ssh/klucze/user-1-github
$ ssh-add ~/.ssh/klucze/user-2-github
```

Wyświetlenie dodanych kluczy
```
$ ssh-add -l
```

Następnie należy sklonować repozytorium dla każdego z użytkowników i skonfigurować ich właściciela
```
$ git clone ....
  
$ git config user.name "nazwa_użytknowika_git"
$ git config user.email "adres@email.pl"
```

Taka konfiguracja powinna zadziałać, jeśli jednak wystąpią problemy to możemy dokonać dalszej konfiguracji w pliku ~/.ssh/config oraz dodać zdalne repozytorium git, gdzie nazwę hosta zamienimy na wpis Host z pliku .ssh/config

```
Host user-1-github
HostName github.com
  User git
  IdentityFile ~/.ssh/klucze/user-1-github
  IdentitiesOnly yes

Host user-2-github
HostName github.com
  User git
  IdentityFile ~/.ssh/klucze/user-2-github
  IdentitiesOnly yes
```
Następnie w repozytorium każdym z repozytorium wydajemy polecenie
```
$ cd repo-user-1
$ git remote add user-1-github git@user-1-github:repozytorium.git

$ cd repo-user-2
$ git remote add user-2-github git@user-2-github:repozytorium.git
```

Można też zmodyfikować plik .git/config zmieniając zmienną url w sekcji [remote "origin"] np.
```
[remote "origin"]
    url = git@<nazwa_hosta_z_ssh_config>:<użytkownik_git>/<repozytorium>.git
```


```
    url = git@user-1-github:sebmd/dot.files.git
```
