# Instalacja Arch Linux

Lista dysków  
```
# fdisk -l
```

Ustawienie typu partycji DOS  
```
# fdisk /dev/sda
p m
o
```

Partycjonowanie dysku (należy pamiętać żeby partycję /boot ustawić jako startową bootable)
```
# cfdisk /dev/sda
```

Tworzenie systemu plików
```
# mkfs.ext4 /dev/sda1
```

Partycja wymiany
```
# mkswap /dev/sda2
# swapon /dev/sda2
```

Zamontowanie nowej partycji
```
# mount /dev/sda1 /mnt
```

Uruchomienie wi-fi
```
# wifi-menu
```

Instalacja podstawowych grup pakietów - dostępne grupy pakietów https://www.archlinux.org/groups/
```
# pacstrap /mnt base base-devel gnome gnome-extra qt5 xorg-drivers
```

Podłączenie partycji /mnt jako głównej
```
# arch-chroot /mnt
```

Ustawienie hasła i dodanie nowego konta
```
# passwd
# useradd -m -g wheel -G users -s /bin/bash red
# passwd red
```

Ustawienie lokalizacji
```
# nano /etc/locale.gen
# locale-gen
```

Ustawienie strefy czasowej
```
# ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
```

Ustawniie nazwy systemu
```
# echo 'x230' > /etc/hostname
```

Instalacja dodatkowych paczek
```
# pacman -S grub-bios os-prober dialog wpa_supplicant net-tools
# pacman -S linux-headers linux-lts linux-lts-headers linux-hardened linux-hardened-headers mc tlp acpi_call firefox networkmanager networkmanager-openvpn network-manager-applet
```

Instalacja grub
```
# grub-install /dev/sda
```

Utworzenie obrazu init
```
# mkinitcpio -p linux
# mkinitcpio -p linux-lts
# mkinitcpio -p linux-hardened
```

Aktualizacja konfiguracji grub
```
# grub-mkconfig -o /boot/grub/grub.cfg
```

Aktuazlizacji ```/etc/fstab```
```
# exit
# genfstab /mnt >> /mnt/etc/fstab
```

Odmontowanie partycji i restart systemu
```
# umount  /mnt
# reboot
```

Po instalacji Network Manager
```
# systemctl enable NetworkManager
# systemctl start NetworkManager
# systemctl enable sshd
# systemctl start sshd
```