# Konfiguracja menadżera plików ranger

## Utworzenie standardowych plików konfiguracyjnych
Poniższe polecenie wytworzy zestaw plików konfiguracyjnych
```
$ ranger --copy-config=all
creating: /home/red/.config/ranger/rifle.conf
creating: /home/red/.config/ranger/commands.py
creating: /home/red/.config/ranger/commands_full.py
creating: /home/red/.config/ranger/rc.conf
creating: /home/red/.config/ranger/scope.sh

> Please note that configuration files may change as ranger evolves.
  It's completely up to you to keep them up to date.

> To stop ranger from loading both the default and your custom rc.conf,
  please set the environment variable RANGER_LOAD_DEFAULT_RC to FALSE.
```
