# Edycja plików Markdown w edytorze Vim
**Uwaga, dokument jest nadal rozwijany.**

## Wymagania
* vim
* pandoc
* qutebrowser
* inotify-tools
* plugin plasticboy/vim-markdown
* plugin godlygeek/tabular

## Podstawowa konfiguracja

## Zasada działania
Podstawowe funkcje to kompilacja pliku markdown do postaci `html` oraz
uruchomienie przeglądarki qutebrowser z tym plikiem.

Drugim elementem jest automatyczne odświeżenie strony `html` w przeglądarcie po
każdej wykonanej konwersji do pliku `html`. służy do tego program
`inotifywait`, który sprawdza czy plik został zmieniony a następnie uruchamia
kolejny skrypt w języku python, który ma za zadanie wysłanie polecenia do
przeglądarki aby ta odświeżyła stronę.

## Instalacja pluginów Vim
Jak można zauważyć instalacja jest wykonana za pomocą menadżera Vundle
```
plugin 'plasticboy/vim-markdown'
plugin 'godlygeek/tabular'
```

```
:so %
:VundleInstall
```

## Automatyzacja podglądu
Jest to rozwiązanie które się sprawdza przy edycji pojedynczych plików

Konfiguracja edytora vim
```
map <leader>p :!qutebrowser <c-r>%<backspace><backspace><backspace>.html &<cr><cr>
map <leader>c :w<cr>:!compiler <c-r>%<cr><cr>
```

Kiedy naciśniemy przycisk `\p` uruchomi się przeglądarka qutebrowser dla wygenerowanego
pliku `.md.html`

Skrypt `compiler` to skrypt napisany w bash gdzie za pomocą programu `pandoc`
zostaje przekonwertowany na plik `html`.
```
#!/bin/bash

file=$(basename "$1")
ext="${file##*.}"
base="${file%.*}"
full_path_file=$(readlink -f $1)
filedir=$(dirname $full_path_file)

case "$ext" in
  md)
    pandoc "$full_path_file" -t html -o $filedir/$base.html
    ;;
esac
```

Wyłączenie zawijania (folding)
```
let g:vim_markdown_folding_disabled = 1
```


### Monitorowanie zmian w pliku
Skrypt `monitorowanie.sh` wymaga podania nazwy pliku lub katalogu jaki będzie
monitorowany pod kątem zmian. Wywołanie skryptu: `$ ./monitorowanie.sh plik.html`
```bash
#!/bin/bash

script=$(readlink -f $1)
inotifywait -m $script -q -e close_write |
while read file path action
do
  ~/bin/qute_reload.py
done
```

## Skrypt odświeżający stronę w qutebrowser
skrypt `reload-page.py`
```python
#!/usr/bin/python3

# https://pastebin.com/wdinmbm8


try:
    from qutebrowser import qutebrowser, app
    from qutebrowser.misc import ipc
except importerror:
    print("error: qutebrowser missing.")
    exit(1)


def qute_reload():
    """send config-source command to qutebrowsers ipc server."""
    args = qutebrowser.get_argparser().parse_args()
    app.standarddir.init(args)
    socket = ipc._get_socketname(args.basedir)
    ipc.send_to_running_instance(socket, [":reload"], args.target)

qute_reload()
```

## Typowy workflow
* Uruchamiamy skrypt ./monitorowanie plik.md
* vim plik.md
* edytujemy plik
* przechodzimy do kompilacji `\c`, kompilacja automatycznie zapisuje plik .md
* otwieramy przeglądarkę qutebrowser `\p`
* teraz po każdej kompilacji automatycznie odświeży się strona w przeglądarce
qutebrowser
