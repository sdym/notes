# Arch Pakiety
* xorg
* xorg-drivers
* xorg-init
* xorg-xbacklight
* linux
* linux-hardened
* linux-headers
* linux-hardened-headers
* tmux
* mc
* ranger
* os-prober
* openssh
* grub-install
* links
* dialog
* firefox
* dmenu
* rofi
* neovim
* mupdf
* neomutt
* screenfetch
* the_silver_searcher (ag)  https://github.com/ggreer/the_silver_searcher wyszukiwanie plików można go zintegrować z pluginem ctrlp dla edytora Vim
* screenkey

# Terminale
* termite
* tilix
* terminator
* sakura
* st
* gnome-terminal

# Graficzne menadżery plików
* pcmanfm
* nemo
* nautilus
