# Obsługa edytora Vim

## Informacje podstawowe
Strona domowa edytora Vim [www.vim.org](https://www.vim.org)

Uruchomienie pomocy  
`:help` lub `:h`

Pomoc do konkretnego polecenia  
`:help deleting`

## Tryby edytora
Standardowo edytor jest uruchamiany w trybie normalnym czyli możemy używać
komend ale nie możemy wprowadzać tekstu. Za pomocą przycisku `Esc` możemy
opuścieć tryb wprowadzania czy wizualny.

W tryb edycji możemy wejść na wiele sposobów np za pomocą `i` czy `a` więcej na
ten temat w sekcji wprowadzanie tekstu i edycja.

| skrót    | opis                        |
|----------|-----------------------------|
| `i`      | wejście w tryb wprowadzania |
| `v`      | tryb visual                 |
| `V`      | tryb visual liniowy         |
| `Ctrl-v` | tryb visual blokowy         |
| `R`      | tryb zastępowania           |
| `:`      | tryb wprowadzania komend    |

## Otwarcie pliku do edycji
Plik można otworzyć używając jego nazwy jako parametru  
`$ vim nazwa_pliku.txt`

Jeśli mamy już uruchomiony edytor to możemy go otworzyć za pomocą polecenia
`:e`  
`:e nazwa_pliku.txt`

## Poruszanie się po ekranie
Oczywiście można używać kursorów ale po jakimś czasie może sie to okazać
niewygodne. Vim w tybie normalnym używa następujących skrótów.

Podsawy  

| skrót | opis                            |
|-------|---------------------------------|
| `j`   | przechodzi do linii niżej       |
| `k`   | przechodzi do linii wyżej       |
| `h`   | przechodzi o jeden znak w lewo  |
| `l`   | przechodzi o jeden znak w prawo |


Inne skróty

| skrót    | opis                                                              |
|----------|-------------------------------------------------------------------|
| `h`      | znak w lewo                                                       |
| `j`      | linia niżej                                                       |
| `k`      | linia wyżej                                                       |
| `l`      | znak w prawo                                                      |
| `H`      | skocz do góry ekranu                                              |
| `M`      | skocz do połowy ekranu                                            |
| `L`      | skocz do dołu ekranu                                              |
| `gg`     | skocz na początek pliku                                           |
| `G`      | skocz na koniec pliku                                             |
| `%`      | skocz do następnego pasującego znaku klamrowego                   |
| `12G`    | skocz do linii 12                                                 |
| `:12`    | skocz do linii 12                                                 |
| `14h`    | 14 znaków w lewo                                                  |
| `14l`    | 14 znaków w prawo                                                 |
| `14j`    | 14 linii w dół                                                    |
| `14k`    | 14 linii w górę                                                   |
| `14H`    | 14 linii od góry ekranu                                           |
| `12L`    | 12 linii od dołu ekranu                                           |
| `:3-`    | przejście powyżej 3 linii czyli do linii 2                        |
| `:3+`    | przejście poniżej 3 linii czyli do linii 4                        |
| `zz`     | ustawienie kursora na środku ekranu                               |
| `Ctrl+e` | przesuwa ekran o jedną linię do góry bez zmiany położenia kursora |
| `Ctrl+y` | przesuwa ekran o jedną linię w dół bez zmiany położenia kursora   |
| `Ctrl+b` | przesuwa ekran o całą jego wysokość do góry                       |
| `Ctrl+f` | przesuwa ekran o całą jego wysokość w dół                         |
| `Ctrl+u` | przesuwa ekran o połowę tekstu w górę                             |
| `Ctrl+d` | przesuwa ekran o połowę tekstu w dół                              |
| `{`, `}` | poruszanie się pomiędzy paragrafami                               |

## Poruszanie się w linii
| skrót    | opis                                                                         |
|----------|------------------------------------------------------------------------------|
| `h`      | znak w lewo                                                                  |
| `l`      | znak w prawo                                                                 |
| `e`      | przeskakuje na koniec kolejnego wyrazu                                       |
| `E`      | przeskakuje na koniec kolejnego wyrazu z pominięciem znaków specjalnych      |
| `2e`     | przeskakuje na koniec drugiego wyrazu                                        |
| `w`      | przeskakuje na początek kolejnego wyrazu                                     |
| `W`      | przeskakuje na początek kolejnego wyrazu z pominięciem znaków specjalnych    |
| `2w`     | przeskakuje o dwa wyrazy na początek trzeciego                               |
| `b`      | przeskakuje na początek poprzedniego wyrazu                                  |
| `B`      | przeskakuje na początek poprzedniego wyrazu z pominięciem znaków specjalnych |
| `2b`     | przeskakuje 2 wyrazy wcześniej na początek wyrazu                            |
| `ge`     | przeskakuje na koniec poprzedniego wyrazu                                    |
| `gE`     | przeskakuje na koniec poprzedniego wyrazu z pominięciem znaków specjalnych   |
| `2ge`    | przeskakuje 2 wyrazy wcześniej na koniec wyrazu                              |
| `0`      | przeskakuje na początek linii                                                |
| `$`      | przeskakuje na koniec linii                                                  |
| `^`, `_` | przeskakuje na początek linii do pierwszego nie pustego znaku                |
| `g_`     | przeskakuje do osatniego niepustego znaku w linii                            |
| `-`      | przeskakuje na początek poprzedniej linii                                    |
| `14h`    | 14 znaków w lewo                                                             |
| `14l`    | 14 znaków w prawo                                                            |

## Wprowadzanie tekstu i edycja

| skrót     | opis                                                             |
|-----------|------------------------------------------------------------------|
| `i`       | wstaw przed kursorem                                             |
| `I`       | wstaw na początku wiersza                                        |
| `a`       | wstaw za kursorem                                                |
| `A`       | wstaw na końcu wiersza                                           |
| `o`       | wstaw nową linię poniżej                                         |
| `O`       | wstaw nową linię powyżej                                         |
| `~`       | zmiana wielkości litery pod kursorem                             |
| `ea`      | wstaw na końcu wyrazu                                            |
| `bi`      | wstaw na początku wyrazu                                         |
| `cw`      | usunięcie wyrazu i wejście do trybu wprowadzania                 |
| `r'znak'` | zmiana znaku w miejscu kursora np `rc`                           |
| `C`, `c$` | usuwa wiersz od miejsca kursora i przechodzi w tryb wprowadzania |
| `S`, `cc` | usuwa cały wiersz i przechodzi w tryb wprowadzania               |
| `2S`      | usuwa dwie linie i przechodzi w tryb wprowadzania                |
| `R`       | wchodzi w tryb REPLACE zamiany tekstu                            |
| `s`       | usuwa znak i przechodzi w tryb wprowadzania                      |
| `2s`      | usuwa dwa znaki i przechodzi w tryb wprowadzania                 |
| `J`       | łączy dwie linie bieżącą z następną                              |
| `xp`      | zamienia znaki miejscami                                         |
| `X`       | usunięcie znaku przed kursorem                                   |
| `dd`      | usunięcie całej linii                                            |
| `D`       | usunięcie linii od kursora w prawo                               |
| `J`       | połączenie linii ze spacją pomiędzy liniami                      |
| `gJ`      | połączenie linii bez spacji pomiędzy liniami                     |
| `14d`     | usunięcie 14 linii w buforze                                     |
| `dk`      | usunięcie bieżącej linii i linii powyżej                         |
| `dj`      | usunięcie bieżącej linii i linii poniżej                         |
| `Ctrl+a`  | dodaje 1 do liczby na której znajduje się kursor                 |
| `Ctrl+x`  | odejmuje 1 od liczby na której znajduje się kursor               |

## Praca w trybie wizualnym

| skrót              | opis                                                                                                          |
|--------------------|---------------------------------------------------------------------------------------------------------------|
| `v`                | tryb visual                                                                                                   |
| `V`                | tryb visual liniowy                                                                                           |
| `Ctrl-v`           | tryb visual blokowy                                                                                           |
| `ESC`              | wyjście z trybu wizualnego                                                                                    |
| `d`                | kasuje zaznaczony tekst                                                                                       |
| `D`                | kasuje wiersz z zaznaczonym tekstem                                                                           |
| `y`                | kopiuje zaznaczony tekst                                                                                      |
| `Y`                | kopiuje wiersz z zaznaczonym tekstem                                                                          |
| `c`                | usuwa zaznaczony tekst i przechodzi w tryb wprowadzania                                                       |
| `C`                | usuwa cały wiersz z zaznaczonym tekstem i przechodzi w tryb wprowadzania                                      |
| `vap`              | zaznacza cały paragraf                                                                                        |
| `dap`              | zaznacza cały paragra i usuwa go / wycina                                                                     |
| `cap`              | zaznacza cały paragraf, usuwa go i przechodzi w tryb wprowadzania                                             |
| `yap`              | kopiuje cały paragraf                                                                                         |
| `vi(`              | zaznacza cały tekst w nawiasach w tym wypadku `(`                                                             |
| `va[`              | zaznacza cały tekst i nawiasy                                                                                 |
| `vi[d`             | zaznacza cały tekst w nawiasach `[` i usuwa go                                                                |
| `vi(c` + `[tekst]` | zaznacza tekst w nawiasach `(` i przechodzi w tryb wprowadzania, zastępuje operację znazacz, usuń, wprowadzaj |
| `vi[r + [znak]`    | zaznacza tekst w nawiasach `[` a następnie cały tekst zastępuje podanym znakiem np. `[************]`          |


## Kopiowanie, wycinanie, wklejanie, usuwanie
| skrót       | opis                                                    |
|-------------|---------------------------------------------------------|
| `yy`, `Y`   | kopiuje całą linię                                      |
| `yw`        | kopiuje wyraz                                           |
| `3Y`, `3yy` | kopiuje 3 linie                                         |
| `y1p`       | kopiuje bieżącą linię oraz jedną linię w dół            |
| `y1k`       | kopiuje bieżącą linię oraz jedną linię w górę           |
| `y$`        | kopiuje od kursora do końca linii                       |
| `dd`        | wycięcie jednej linii                                   |
| `dw`        | wycięcie jendego wyrazu od kursora oraz spacje za nim   |
| `de`        | wycięcie jednego wyrazu od kursora ale zostawia spację  |
| `dk`        | usuwa bieżącą linię i linię powyżej                     |
| `dj`        | usuwa bieżącą linię i linię poniżej                     |
| `d2w`       | usuwa dwa słowa                                         |
| `d3w`       | usuwa trzy słowa                                        |
| `3dj`       | usuwa 3 linie                                           |
| `D`         | wycięcie całej linii pozostaje w miejscu wyciętej linii |
| `2D`, `2dd` | wycięcie dwóch linii                                    |
| `d$`        | wycina do końca wiersza                                 |
| `x`         | wycina jeden znak                                       |
| `X`         | wycina znak przed kursorem                              |
| `p`         | wklej za kursorem                                       |
| `P`         | wklej przed kursorem                                    |


## Wyszukiwanie
| skrót     | opis                                         |
|-----------|----------------------------------------------|
| `f[znak]` | przechodzi do kolejnego wystąpienia znaku    |
| `F[znak]` | przechodzi do poprzedniego wystąpienia znaku |


## Zapisywanie i wychodzenie z edytora

| skrót         | opis                                             |
|---------------|--------------------------------------------------|
| `:w`          | zapisanie zmian                                  |
| `:w plik`     | zapisuje jako plik                               |
| `:wq`         | zapisz i wyjdź                                   |
| `q`           | wyjście z edytora                                |
| `q!`          | wyjście z edytora bez zapisywania zmian          |
| `:x`          | zapisanie i wyjście z edytora                    |
| `ZZ`          | zapisanie i wyjście z edytora                    |
| `ZQ`          | wyjdź i porzuć zmiany                            |
| `1,12w plik ` | zapisuje zakres od linii 1 do 12 w osobnym pliku |


## Wyświetlenie wersji edytora
```
:version
```


## Plik konfiguracyjny edytora Vim
Plik znajduje się bezpośrednio w katalogu domowym użytkownika: `~/.vimrc`


## Uruchomienie Vim z inną konfiguracją
Wczytanie konfiguracji z innego pliku niż `.vimrc` jest możliwe za pomocą
parametru `-u plik-konfiguracyjny`  
```
$ vim -u ~/.vimrc-markdown
```

Jeśli chcemy uruchomić edytor bez wczytania konfiguracji to używamy `NONE` jako
nazwa pliku konfiguracyjnego  
```
$ vim -u NONE
```


## Uruchomienie skryptu / przeładowanie konfiguracji .vimrc po edycji
W sytuacji kiedy edytujemy jakiś plik np. skrypt shell, możemy go uruchomić za
pomocą komendy: `:!%`, jeśli chodzi o przeładowanie konfiguracji Vim wpisujemy
`source %` jak można się domyślić znak `%` zastępuje nazwę pliku który
aktualnie edytujemy.


## Numerowanie linii
```
:set nu
```


## Wstawienie listy plików do bufora
```
:read !ls ~/Pictures/*.png
:r! ls ~/Pictures/*.png
```


## Wstawienie bieżącej daty
```
:r! date
```


## Wstawienie zawartości innego pliku
```
:r plik.txt
```


## Usunięcie wszystkich pustych linii
```
:g/^$/d
```


## Usunięcie wszystkich linii zawierających podany wyraz
```
:v/wyraz/d
```


## Zamiana odstępów tabulatora na spacje
```
:retb
```


## Sortowanie
| Polecenie     | Opis                                        |
|---------------|---------------------------------------------|
| `:sort`       | sortuje cały plik                           |
| `:sort!`      | sortuje cały plik w odwrotnej kolejności    |
| `:sort i`     | sortuje cały plik ignorując wielkość znaków |
| `:sort u`     | usuwa zduplikowane linie                    |
| `:'<,'>!sort` | sortuje zaznaczony tekst                    |


## Wstawienie tekstu na początku lub na końcu linii w całym pliku
Znajdując się na początku pliku zaznaczamy całość do końca pliku za pomocą komendy ```V``` a następnie ```G```, komenda ```V``` zmienia tryb na wizualny liniowy ```V-LINE```, komenda ```G``` przechodzi do końca pliku, dzięki czemu cały plik zostanie zaznaczony. Teraz uruchamiamy polecenie ```:norm```, dzięki któremu będziemy mogli wprowadzać kolejne polecenia.
```
:'<,'>norm Itekst do wprowadzenia
```
komenda `I` służy do wprowadzenia tekstu na początku linii

Jeśli chcemy wprowadzić tekst na końcu linii należy użyć komendy ```A```
```
:'<,'>norm Atekst do wprowadzenia
```

Znacznnik `'<,'>` jest dodawany automatycznie.

## Skopiowanie tekstu pomiędzy znakami "
Załóżmy że mamy taką linię:
```
<a href="https://www.github.com">Github</a>
```
Żeby skopiować link do strony możemy to zrobić za pomocą sekwencji komend
```
0yi"
```

## Otworzenie linku w przeglądarce
Korzystając z poprzedniej sekwencji ```0yi"``` mamy skopiowany link teraz możemy go otworzyć, do tego zmapujemy skrót klawiszowy ```<leader>p``` w moim przypadku ```<leader>``` to znak ```\```. W pliku ```.vimrc``` dodajemy
```
map <leader>p 0yi":!firefox <C-R>" & disown<CR><CR>
```

## Wyszukiwanie znacznika i zamienienie go tekstem
Wykonując automatyczne dodawanie tekstu możemy dodać gdzieś znacznik np. ```<++>``` jeśli umieścimy ten znacznik w wielu liniach możemy przeskakiwać do następnego wystąpienia znacznika i przechodzić w tryb wprowadzania za pomoća zmapowanej sekwencji. Niech to będzie ```Spacja+Tab```.

W pliku ```.vimrc``` dodajemy
```
inoremap <Space><Tab> <Esc>/<++><Enter>"_c4l
vnoremap <Space><Tab> <Esc>/<++><Enter>"_c4l
map <Space><Tab> <Esc>/<++><Enter>"_c4l
```
Teraz używając sekwencji ```Spacja+Tab``` edytor przejdzie do kolejnego wystąpienia znacznika ```<++>``` a następnie usunie go i przejdzie w tryb wprowadzania.

## Źródła informacji
* [Kanał YT Luke Smith](https://www.youtube.com/channel/UC2eYFnH61tmytImy1mTYvhA/videos)
