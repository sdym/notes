# Backup danych na szyfrowanym nośniku
Skrypty używają jako nośnika kopii zapasowej karty SD: ```/dev/mmcblk30```, jest
on ustawiony w parametrze ```Device``` należy zmienić na właściwy dla swojego
systemu.


## Skrypt formatujący nośnik z obsługą szyfrowania
```bash
#!/bin/bash

CryptoName=CryptoSD
Device=/dev/mmcblk30

if [[ $EUID -ne 0 ]]; then
  echo "This program must be run as root" 1>&2
  exit 1
fi

cryptsetup luksFormat -s 512 -h sha512 -i 333 $Device
cryptsetup luksOpen $Device $CryptoName
mkfs.ext4 /dev/mapper/$CryptoName
sleep 3
cryptsetup close $CryptoName
```

## Skrypt montujący szyfrowany nośnik
```bash
#!/bin/bash

CryptoName=CryptoSD
Device=/dev/mmcblk30

if [[ $EUID -ne 0 ]]; then
    echo "This program must be run as root" 1>&2
    exit 1
fi

/sbin/cryptsetup luksOpen $Device $CryptoName
/bin/mount /dev/mapper/$CryptoName /home/user/$CryptoName
```

## Skrypt odłączający szyfrowany nośnik
```bash
#!/bin/bash

CryptoName=CryptoSD
Device=/dev/mmcblk30

if [[ $EUID -ne 0 ]]; then
    echo "This program must be run as root" 1>&2
    exit 1
fi

partition=$(mount | grep $CryptoName | awk '{ print $3 }')
crypto=$(ls /dev/mapper/ | grep $CryptoName)

if [ -n "$partition" ]
then
    echo -n "Synchronizacja... "
    /bin/sync && echo [OK]
    echo -n "Odmontowuje partycje... "
    /bin/umount $partition && echo [OK]
fi

if [ -n "$crypto" ]
then
    echo -n "Zamykam crypto... "
    /sbin/cryptsetup luksClose $CryptoName && echo [OK]
fi
```

## Skrypt wykonujący kopię zapasową na szyfrowanym nośniku
Skrypt wykonuje kopię zapasową wybranych katalogów z pominięciem katalogów 
zawartych w pliku ```/home/user/bin/backup2sd.exclude``` który jest ustawiony
w zmiennej ```ExcludeFile```, każdy katalog znajduje się w oddzielnej linii.

```bash
#!/bin/bash

### Ustawienia
CryptoName=CryptoSD
Device=/dev/mmcblk30
ExcludeFile=/home/user/bin/backup2sd.exclude

if [[ $EUID -ne 0 ]]; then
    echo "This program must be run as root" 1>&2
    exit 1
fi

### Montowanie
/sbin/cryptsetup luksOpen $Device $CryptoName
/bin/mount /dev/mapper/$CryptoName /home/user/$CryptoName

### Backup
rsync -a -v --exclude-from=$ExcludeFile /home/user /home/user/CryptoSD
rsync -a /etc /home/user/CryptoSD

### Odmontowanie
partition=$(mount | grep $CryptoName | awk '{ print $3 }')
crypto=$(ls /dev/mapper/ | grep $CryptoName)

if [ -n "$partition" ]
then
    echo -n "Synchronizacja... "
    /bin/sync && echo [OK]
    echo -n "Odmontowuje partycje... "
    /bin/umount $partition && echo [OK]
fi

if [ -n "$crypto" ]
then
    echo -n "Zamykam crypto... "
    /sbin/cryptsetup luksClose $CryptoName && echo [OK]
fi
```

Przykładowy plik ```/home/user/bin/backup2sd.exclude```
```
.Private
.PyCharmCE2017.3
.cmake
.cache
.dbus
.ecryptfs
.gimp-2.8
```
