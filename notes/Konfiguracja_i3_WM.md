# Konfiguracja i3 WM
Plik konfiguracyjny i3 znajduje się w `.config/i3/config` lub `.i3/config`

## Ustawienie klawisze $mod
Znak Super / Win
```
set $mod Mod4
```

Alt
```
set $mod Mod1
```

## Ustawienie standardowego terminala
```
set $term /usr/bin/termite
```

## Przełączanie się pomiędzy dwoma ostatnimi pulpitami
Konfiguracja skrótu `Mod + TAB`
```
bindsym $mod+Tab workspace back_and_forth
```

## Usunięcie górnej belki z tytułem okna
```
for_window [class="^.*"] border pixel 1
```

## Przeniesienie okna (kontenera) na inny pulpit
Przenosi okno na inny pulpit
```
bindsym $mod+shift+1 move container to workspace $ws1
```

Przenosi okno na inny pulpit i przełącza się na niego
```
bindsym $mod+shift+1 move container to workspace $ws1; workspace $ws1
```

## Badanie okna aplikacji pod katem nazwy, klasy
```
 xprop
_NET_WM_USER_TIME(CARDINAL) = 1006682
WM_STATE(WM_STATE):
                window state: Normal
                icon window: 0x0
_NET_WM_DESKTOP(CARDINAL) = 1
I3_FLOATING_WINDOW(CARDINAL) = 1
WM_HINTS(WM_HINTS):
                Client accepts input or input focus: True
                Initial state is Normal State.
                window id # of group leader: 0x1800001
_GTK_THEME_VARIANT(UTF8_STRING) = 
XdndAware(ATOM) = BITMAP
_NET_WM_WINDOW_TYPE(ATOM) = _NET_WM_WINDOW_TYPE_NORMAL
_NET_WM_SYNC_REQUEST_COUNTER(CARDINAL) = 25165829, 25165830
_NET_WM_USER_TIME_WINDOW(WINDOW): window id # 0x1800004
WM_CLIENT_LEADER(WINDOW): window id # 0x1800001
_NET_WM_PID(CARDINAL) = 828
WM_LOCALE_NAME(STRING) = "pl_PL.UTF-8"
WM_CLIENT_MACHINE(STRING) = "x230"
WM_NORMAL_HINTS(WM_SIZE_HINTS):
                program specified minimum size: 10 by 19
                program specified base size: 0 by 0
                window gravity: NorthWest
WM_PROTOCOLS(ATOM): protocols  WM_DELETE_WINDOW, WM_TAKE_FOCUS, _NET_WM_PING, _NET_WM_SYNC_REQUEST
WM_CLASS(STRING) = "dropdown", "Termite"
WM_ICON_NAME(STRING) = "1:1:xprop - "user@x230:~" "
_NET_WM_ICON_NAME(UTF8_STRING) = "1:1:xprop - \"user@x230:~\" "
WM_NAME(STRING) = "1:1:xprop - "user@x230:~" "
_NET_WM_NAME(UTF8_STRING) = "1:1:xprop - \"user@x230:~\" "
```
