# Włączenie pluginu dla wybranych typów plików

Dla menadżera pluginów Plug składnia wyląda w ten sposób

```vim
Plug 'davidhalter/jedi-vim', {'for': 'python'}
```

Możemy też uruchomiać plugin dla wielu typów plików

```vim
Plug 'kovisoft/paredit', { 'for': ['clojure', 'scheme'] }
```

Menadżer pluginów Plug jest dostępny na stronie https://github.com/junegunn/vim-plug